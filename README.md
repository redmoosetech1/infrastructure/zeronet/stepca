# Smallstep CA
----
Currently, this Certificate Authority is being deployed to Kubernetes. To get this going I had to customize a few things.
0. VERY IMPORTANT READ!!!!
    - You need to create your certificate with alternate names for the service dns name inside of the cluster!
    - ca.rmt,step-certificates.smallstep.svc.cluster.local,127.0.0.1
1. I needed to create my own password instead of letting the CA generate one. This removed any error from copying characters from the terminal.
2. I had to create an custom PVC for the container to bind to.
3. I had to create expose through LoadBalancer Service.
    - looking to expose through ingress controller. However, we need to bootstrap it through LoadBalancer.
4. Push in custom namespace.

```bash
$ step ca certificate ca.rmt srv.crt srv.key
```


#### Step CLI Installation
----
```bash
$ wget https://dl.smallstep.com/gh-release/cli/docs-ca-install/v0.23.2/step-cli_0.23.2_amd64.deb
$ sudo dpkg -i step-cli_0.23.2_amd64.deb
```


#### TLDR
----
```bash
$ helm repo add smallstep https://smallstep.github.io/helm-charts/
$ step ca init --ssh --acme --helm > values.yaml
$ echo "password" | base64 > password.txt
$ helm install -f values.yaml \
     --set inject.secrets.ca_password=$(cat password.txt) \
     --set inject.secrets.provisioner_password=$(cat password.txt) \
     --set service.targetPort=9000 \
     --set ca.db.existingClaim=stepca-pvc \
     --set service.type=LoadBalancer \
     --namespace smallstep \
     step-certificates smallstep/step-certificates 
```

#### Installing via Charts
----
```bash
$ helm install my-release smallstep/step-certificates
```

#### Uninstalling via Charts
----
```bash
$ helm uninstall my-release
```

#### selinux issue
----
```bash
$ setsebool -P httpd_can_network_connect 1
```


#### References
----
- https://stackoverflow.com/questions/23948527/13-permission-denied-while-connecting-to-upstreamnginx
- https://artifacthub.io/packages/helm/smallstep/step-certificates
- https://smallstep.com/docs/step-ca/installation/#kubernetes
