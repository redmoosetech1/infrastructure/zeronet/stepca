#!/bin/bash

step ssh certificate --provisioner server --insecure --no-password \
    --principal "${HOSTNAME}.rmt" \
    --principal "$(hostname -i) \
    --host \
    "${HOSTNAME}" \
    "/etc/ssh/${HOSTNAME}_ecdsa_key"
