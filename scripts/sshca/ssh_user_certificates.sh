#!/bin/bash

step ssh certificate --provisioner server \
    --principal administrator \
    "nate@redmoose.tech" \
    "$HOME/.ssh/nate_ecdsa"
