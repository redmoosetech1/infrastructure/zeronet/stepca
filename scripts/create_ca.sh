#!/bin/bash

ROOT_CA_DIR="/root/ca/authority"
INTERMEDIATE_DIRS=(
"intermediate"
"kubernetes_intermediate"
"vsphere_intermediate"
)

function setup_ca_dir {
    cp "$CURRENT_DIR/openssl.cnf" "$1/openssl.cnf"
    sed -i 's-CHANGEME-'"${1}"'-g' "$1/openssl.cnf"
    cd "$1" || exit 1
    mkdir certs crl newcerts private csr
    chmod 700 private
    touch index.txt
    echo 1000 > serial
    cd "$CURRENT_DIR" || exit 1
}

CURRENT_DIR=$(pwd)

# Create Root CA
mkdir -p "$ROOT_CA_DIR"
setup_ca_dir "$ROOT_CA_DIR"

# Create Intermediate CA's
for dir in "${INTERMEDIATE_DIRS[@]}"; do
    mkdir -p "$ROOT_CA_DIR/$dir"
    setup_ca_dir "$ROOT_CA_DIR/$dir"
done
