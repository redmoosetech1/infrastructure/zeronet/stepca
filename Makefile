export VAULT_ADDR="https://vault.rmt"
export CI_REGISTRY_IMAGE=gitlab-registry.rmt:443/infrastructure/zeronet/stepca
export CI_COMMIT_BRANCH=main
export APP_ROLE_SECRET=$(vault write -field=secret_id -f auth/approle/role/stepca/secret-id)
STEP_IMAGE_NAME=$(CI_REGISTRY_IMAGE)
STEP_VERSION=$(CI_COMMIT_BRANCH)

.PHONY: build deploy destroy
default: build

build:
	docker build --rm --pull --no-cache -t $(STEP_IMAGE_NAME):$(STEP_VERSION) -t $(STEP_IMAGE_NAME):latest ./stepca
	docker push $(STEP_IMAGE_NAME):$(STEP_VERSION)
	docker push $(STEP_IMAGE_NAME):latest

deploy:
	docker-compose rm -sf
	docker-compose pull
	docker-compose up -d

destroy:
	docker-compose down

test:
