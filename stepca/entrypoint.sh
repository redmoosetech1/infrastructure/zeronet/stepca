#!/bin/bash
RED='\033[38;5;202m'
GREEN='\033[38;5;70m'
NC='\033[0m' # No Color
BOLD='\033[1m'


log_output() {
  level="${1}"
  message="${2}"
  printf "$(date +"[%Y-%m-%d %H:%M:%S]") : %b : %b\n" "${level}"  "${message}"
}

log_info() {
  local message="${1}"
  log_output "INFO" "${GREEN}${BOLD}${message}${NC}"
}

log_error() {
  local message="${1}"
  log_output "${RED}${BOLD}ERROR${NC}" "${message}"
}

die() {
    local message=$1
    log_error "$message" >&2
    exit 1
}

get_stepca_creds() {
  log_info "Getting Admin Password from Vault ..."
  cat << EOF > /tmp/payload.json
  {
    "role_id": "$APP_ROLE_ID",
    "secret_id": "$APP_ROLE_SECRET"
  }
EOF

  APP_DATA=$(curl -s -X POST --data @/tmp/payload.json "${VAULT_ADDR}/v1/auth/approle/login")
  log_info "APP_DATA: ${APP_DATA}"
  APP_TOKEN=$(echo "${APP_DATA}" | jq -r .auth.client_token)
  if [[ -z "$APP_TOKEN" ]]; then
    die "Variable APP_TOKEN is empty!"
  fi

  log_info "APP Token: $APP_TOKEN"
  STEP_PASSWD=$(curl -s -H "X-Vault-Token: $APP_TOKEN" "$VAULT_URL" | jq -r .data.data.password)
  if [[ -z "$STEP_PASSWD" ]]; then
    die "Variable STEP_PASSWD Is Empty ..."
  fi
  log_info "STEP_PASSWD: $STEP_PASSWD"
  rm /tmp/payload.json
}

if [[ -z "$VAULT_ADDR" ]]; then
  die "App role id empty .."
fi


if [[ -z "$APP_ROLE_ID" ]]; then
  die "App role id empty .."
fi

if [[ -z "$APP_ROLE_SECRET" ]]; then
  die "App role secret empty .."
fi

VAULT_URI=${VAULT_URI:=/v1/stepca/data/server}
VAULT_URL="${VAULT_ADDR}${VAULT_URI}"

log_info "RMT Smallstep CA Starting ..."
log_info "VAULT_ADDR: $VAULT_ADDR"
log_info "APP_ROLE_ID: $APP_ROLE_ID"
log_info "APP_ROLE_SECRET: $APP_ROLE_SECRET"
get_stepca_creds
/usr/local/bin/step-ca --password-file=<(echo -n "$STEP_PASSWD") $CONFIGPATH